<?php

    header("Access-Control-Allow-Origin: *");
    header("Content-Type: application/json; charset=UTF-8");
    header("Access-Control-Allow-Methods: POST");
    header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

    include_once '../config/database.php';
    include_once '../class/tasks.php';

    $database = new Database();
    $db = $database->getConnection();
   
    $item = new Task($db);
    $data = json_decode(file_get_contents("php://input"));

    if(!empty($data->name)) {
        $item->name = $data->name;
        $item->status_id = $data->status_id;
        $item->description = $data->description;
        $item->file = $data->file;
        $item->deadline = $data->deadline;
        $item->urgent = $data->urgent;
        $item->user_id = $data->user_id;
        $item->completed = $data->completed;
        $item->created = date('Y-m-d H:i:s');
        
        if($item->createTask()){
            http_response_code(201);
            echo json_encode("Task created successfully.");
        } else{
            http_response_code(406);
            echo json_encode("Task could not be created.");
        }
    } else{
        http_response_code(406);
        echo json_encode("Task could not be created. Fill in the 'name' value.");
    }
?>
