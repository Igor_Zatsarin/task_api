<?php

    header("Access-Control-Allow-Origin: *");
    header("Content-Type: application/json; charset=UTF-8");
    header("Access-Control-Allow-Methods: GET");
    header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
    
    include_once '../config/database.php';
    include_once '../class/tasks.php';

    $database = new Database();
    $db = $database->getConnection();
    $items = new Task($db);

    if(isset($_GET['status_id'])){
        if(!empty($_GET['status_id'])){
            $items->status_id = $_GET['status_id'];
            $stmt = $items->getTasksByStatus();
        }else{
            http_response_code(500);
            echo json_encode("Data could not be readed! Fill in the 'status_id' value.");
            die();
        }
    }elseif(isset($_GET['id'])){
        if(!empty($_GET['id'])){
            $items->id = $_GET['id'];
            $stmt = $items->getSingleTask();
        }else{
            http_response_code(500);
            echo json_encode("Data could not be readed! ill in the 'id' value.");
            die();
        }
    }else{
        $stmt = $items->getAllTasks();
    }

    $itemCount = $stmt->rowCount();

    if($itemCount > 0){    
        $taskArr = array();
        $taskArr["body"] = array();
        $taskArr["itemCount"] = $itemCount;
        while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
            extract($row);
            $e = array(
                "id" => $id,
                "name" => $name,
                "status_id" => $status_id,
                "description" => $description,
                "file" => $file,
                "deadline" => $deadline,
                "urgent" => $urgent,
                "user_id" => $user_id,
                "completed" => $completed,
                "created" => $created
            );
            array_push($taskArr["body"], $e);
        }
        http_response_code(200);
        echo json_encode($taskArr);
    }
    else{
        http_response_code(404);
        echo json_encode(
            array("message" => "No record found.")
        );
    }
?>
