<?php

    header("Access-Control-Allow-Origin: *");
    header("Content-Type: application/json; charset=UTF-8");
    header("Access-Control-Allow-Methods: DELETE");
    header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
    
    include_once '../config/database.php';
    include_once '../class/tasks.php';
    
    $database = new Database();
    $db = $database->getConnection();
    
    $item = new Task($db);
    
    $data = json_decode(file_get_contents("php://input"));
    if(!empty($data->id)){
        $item->id = $data->id;
        
        if($item->deleteTask()){
            http_response_code(200);
            echo json_encode("Task deleted.");
        } else{
            http_response_code(500);
            echo json_encode("Task could not be deleted");
        }
    }else{
        http_response_code(500);
        echo json_encode("Data could not be deleted! Fill in the 'id' value.");
    }
?>