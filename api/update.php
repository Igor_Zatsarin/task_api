<?php

    header("Access-Control-Allow-Origin: *");
    header("Content-Type: application/json; charset=UTF-8");
    header("Access-Control-Allow-Methods: PUT");
    header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
    
    include_once '../config/database.php';
    include_once '../class/tasks.php';
    
    $database = new Database();
    $db = $database->getConnection();
    
    $item = new Task($db);
    $data = json_decode(file_get_contents("php://input"));

    if(isset($_GET['id'])){
        if(!empty($_GET['id'])){
            $item->id = $_GET['id'];
            $item->completed = $data->completed;
            
            if($item->updateTaskStatus()){
                http_response_code(200);
                echo json_encode("Task status updated successfully.");
            }else{
                http_response_code(500);
                echo json_encode("Task status could not be updated.");
            }
        }else{
            http_response_code(500);
            echo json_encode("Task status could not be updated! Fill in the 'id' field.");
        }
    }else{    
        // task values
        if(!empty($data->id) && !empty($data->name)) {
            $item->id = $data->id;
            $item->name = $data->name;
            $item->status_id = $data->status_id;
            $item->description = $data->description;
            $item->file = $data->file;
            $item->deadline = $data->deadline;
            $item->urgent = $data->urgent;
            $item->user_id = $data->user_id;
            $item->completed = $data->completed;
            $item->created = date('Y-m-d H:i:s');
            
            if($item->updateTask()){
                http_response_code(200);
                echo json_encode("Task data updated.");
            } else{
                http_response_code(500);
                echo json_encode("Data could not be updated.");
            }
        }else{
            http_response_code(500);
            echo json_encode("Data could not be updated! Fill in the needed data.");
        }
    
    }
?>
