<?php

class Task
{
    // Conncetion
    private $conn;
    // Table
    private $db_table = "tasks";
    // Columns
    public $id;
    public $name;
    public $status_id;
    public $description;
    public $file;
    public $deadline;
    public $urgent;
    public $user_id;
    public $completed;
    public $created;

    // Db connection
    public function __construct($db)
    {
        $this->conn = $db;
    }

    // Get all tasks
    public function getAllTasks()
    {
        $sqlQuery = "SELECT id, name, status_id, description, file, deadline, urgent, user_id, completed, created FROM " . $this->db_table . "";
        $stmt = $this->conn->prepare($sqlQuery);
        $stmt->execute();
        return $stmt;
    }

    // Read single task data
    public function getSingleTask()
    {
        $sqlQuery = "SELECT id, 
                        name, 
                        status_id, 
                        description, 
                        file, 
                        deadline, 
                        urgent, 
                        user_id, 
                        completed, 
                        created 
                    FROM " . $this->db_table . "
                    WHERE id = :id";
        $stmt = $this->conn->prepare($sqlQuery);
        
        // sanitize
        $this->id=htmlspecialchars(strip_tags($this->id));
        // bind data
        $stmt->bindParam(":id", $this->id);
        
        $stmt->execute();
        return $stmt;
     }        

    // Get work or private tasks
    public function getTasksByStatus()
    {
        $sqlQuery = "SELECT id, 
                        name, 
                        status_id, 
                        description, 
                        file, 
                        deadline, 
                        urgent, 
                        user_id, 
                        completed, 
                        created 
                    FROM " . $this->db_table . "
                    WHERE status_id = :status_id";
        $stmt = $this->conn->prepare($sqlQuery);

        // sanitize
        $this->status_id=htmlspecialchars(strip_tags($this->status_id));
        // bind data
        $stmt->bindParam(":status_id", $this->status_id);

        $stmt->execute();
        return $stmt;
    }
    
    // Create task
    public function createTask()
    {
        $sqlQuery = "INSERT INTO
                    ". $this->db_table ."
                    SET
                        name = :name, 
                        status_id = :status_id, 
                        description = :description, 
                        file = :file, 
                        deadline = :deadline,
                        urgent = :urgent,
                        user_id = :user_id,
                        completed = :completed,
                        created = :created";

        $stmt = $this->conn->prepare($sqlQuery);

        // sanitize
        $this->name=htmlspecialchars(strip_tags($this->name));
        $this->status_id=htmlspecialchars(strip_tags($this->status_id));
        $this->description=htmlspecialchars(strip_tags($this->description));
        $this->file=htmlspecialchars(strip_tags($this->file));
        $this->deadline=htmlspecialchars(strip_tags($this->deadline));
        $this->urgent=htmlspecialchars(strip_tags($this->urgent));
        $this->user_id=htmlspecialchars(strip_tags($this->user_id));
        $this->completed=htmlspecialchars(strip_tags($this->completed));
        $this->created=htmlspecialchars(strip_tags($this->created));

        // bind data
        $stmt->bindParam(":name", $this->name);
        $stmt->bindParam(":status_id", $this->status_id);
        $stmt->bindParam(":description", $this->description);
        $stmt->bindParam(":file", $this->file);
        $stmt->bindParam(":deadline", $this->deadline);
        $stmt->bindParam(":urgent", $this->urgent);
        $stmt->bindParam(":user_id", $this->user_id);
        $stmt->bindParam(":completed", $this->completed);
        $stmt->bindParam(":created", $this->created);

        if($stmt->execute()){
            return true;
        }
        return false;
    }

 

    // Update task
    public function updateTask()
    {
        $sqlQuery = "UPDATE
                    ". $this->db_table ."
                    SET
                        name = :name, 
                        status_id = :status_id, 
                        description = :description, 
                        file = :file, 
                        deadline = :deadline,
                        urgent = :urgent,
                        user_id = :user_id,
                        completed = :completed,
                        created = :created
                    WHERE 
                        id = :id";

        $stmt = $this->conn->prepare($sqlQuery);

        $this->name=htmlspecialchars(strip_tags($this->name));
        $this->status_id=htmlspecialchars(strip_tags($this->status_id));
        $this->description=htmlspecialchars(strip_tags($this->description));
        $this->file=htmlspecialchars(strip_tags($this->file));
        $this->deadline=htmlspecialchars(strip_tags($this->deadline));
        $this->urgent=htmlspecialchars(strip_tags($this->urgent));
        $this->user_id=htmlspecialchars(strip_tags($this->user_id));
        $this->completed=htmlspecialchars(strip_tags($this->completed));
        $this->created=htmlspecialchars(strip_tags($this->created));
        $this->id=htmlspecialchars(strip_tags($this->id));

        // bind data
        $stmt->bindParam(":name", $this->name);
        $stmt->bindParam(":status_id", $this->status_id);
        $stmt->bindParam(":description", $this->description);
        $stmt->bindParam(":file", $this->file);
        $stmt->bindParam(":deadline", $this->deadline);
        $stmt->bindParam(":urgent", $this->urgent);
        $stmt->bindParam(":user_id", $this->user_id);
        $stmt->bindParam(":completed", $this->completed);
        $stmt->bindParam(":created", $this->created);
        $stmt->bindParam(":id", $this->id);

        if($stmt->execute()){
            return true;
        }
        return false;
    }

    // Update task status
    public function updateTaskStatus()
    {
        $sqlQuery = "UPDATE
                    ". $this->db_table ."
                    SET
                        completed = :completed
                    WHERE 
                        id = :id";

        $stmt = $this->conn->prepare($sqlQuery);

        $this->completed=htmlspecialchars(strip_tags($this->completed));
        $this->id=htmlspecialchars(strip_tags($this->id));

        // bind data
        $stmt->bindParam(":completed", $this->completed);
        $stmt->bindParam(":id", $this->id);

        if($stmt->execute()){
            return true;
        }
        return false;
    }

    // Delete task
    function deleteTask()
    {
        $sqlQuery = "DELETE FROM " . $this->db_table . " WHERE id = :id";
        $stmt = $this->conn->prepare($sqlQuery);

        $this->id=htmlspecialchars(strip_tags($this->id));
        $stmt->bindParam(":id", $this->id);

        if($stmt->execute()){
            return true;
        }
        return false;
    }
}